select 

  stk.BrandCode
, a.StockCode
, stk.StockDesc
, stk.StatusCode
,ROUND(a.StaffPriceExcGST*1.3,2) AS FriendPriceExcTax	-- +30% on staff 
,ROUND(a.StaffPriceExcGST*1.3*1.1,2) AS FriendPriceIncTax
,a.StaffPriceExcGST
,a.StaffPriceIncGST
,b.WholeSalesExcTax
,ROUND(b.WholeSalesExcTax*1.1,2) AS WholeSalesIncTax
,b.RRPIncTax
,a.AvgCost, a.QtyOnHand
FROM dbo.v_MEY_StockMaster AS stk INNER JOIN
dbo.v_MEY_StockHierarchyGroup AS sg ON stk.ItemGroupCode = sg.GroupCode INNER JOIN
(
	SELECT StockCode, AvgCost, QtyOnHand,
	round(AvgCost*1.2,2) AS [StaffPriceExcGST],	-- +20% on cost
	round(AvgCost*1.2*1.1,2) AS [StaffPriceIncGST]
	FROM dbo.v_MEY_StockWarehouseDetail
	where WhseCode = '3CRO'
	and QtyOnHand > 0
	--and StockCode IN
	--(
	--'840360',
	--'840340',
	--'840500',
	--'460830',
	--'599210',
	--'786460',
	--'840330'
	--)
) AS a ON stk.stockcode = a.stockcode LEFT OUTER JOIN
(
select *
from dbo.v_MEY_StockPrice
where PriceBook = 'IN'
) AS b ON a.StockCode = b.StockCode
where sg.ClassCode NOT IN ('MISC','MERC','SPAR')
order by stk.BrandCode, a.StockCode